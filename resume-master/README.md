# 个人简历模板

## Intro

此简历模板：

- 内容上，来源于我本人真实求职经历以及参加多次网上简历培训总结而成

## Object

前端求职/程序员求职

## Usage

1. 先Star/Fork本项目，然后Clone或者直接下载到本地
2. 修改index.html内相关信息
3. 微调样式（作为前端求职，这点应该不成问题）
4. 生成pdf(开发中)
5. 部署到线上
6. 生成访问二维码（开发中）
7. 祝您求职成功！

## Preview

### PC端
![](assets/images/pc.png)

### 移动端
![](assets/images/ip.png)

## TODO
- [x] 左栏固定（切换）
- [ ] 输出pdf功能
- [ ] 可编辑

## ChangeLog
- 2023.3.7 创建模板
- 2023.3.12 移动端优化
- 2023.12.20 修改部分内容
- 2023.6.20 新增左栏固定功能（切换）

## Acknowledgments
- font-awesome提供字体图标

## LICENSE
